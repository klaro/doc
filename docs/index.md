# Getting started with KlaroCPQ

Table of contents:

#### Introduction

- [Basic Concepts](BasicConcepts.md)
- [User Guide](UserGuide.md)

#### Installation & Configuration

* [Docker Installation](Docker.md)
* [Manual Installation](Installation.md)
* [Configuration](Configuration.md)
* [User Roles](UserRoles.md)
* [Database Configuration](DatabaseConfiguration.md)

#### Usage

- [Quotation Facade](QuotationFacade.md)
- [Searching Quotations](SearchingQuotations.md)
- [Web API](WebApi.md)

#### Customization

* [Entity Formatter](EntityFormatter.md)
* [Creating forms](PhaseReference.md)
* [Translations / Customizing Used Terminology](Translations.md)
* [Offering Summary](OfferingSummary.md)
* [Product Items](ProductItems.md)
* [Output Documents](OutputDocuments.md)
* [Summary Pages](SummaryPages.md)

#### Under the hood

* [Configuration Objects](ConfigurationObjects.md)
* [Event system](Events.md)
* [Phase Form Editor](PhaseFormEditor.md)
* [JavaScript Widgets](JavaScriptModules.md)

#### Development

* [Extending services](ExtendingServices.md)
* [Running tests](Tests.md)
* [Debugging](Debugging.md)