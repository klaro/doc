# Database Configuration

[TOC]

## Introduction

QuotationBundle does not rely on any ORM or database. All interaction is done through interface classes. However, the usage requires somekind of persistance layer and a user management system. It is up to the surrounding application to provide the necessary glue code to map the used interfaces to actual database entities.

## Entity Interfaces

The interfaces that are used to get and store data for quotations, revisions and models are listed below. The interface classes are defined under the namespace `Klaro\QuotationBundle\Api`.

| Interface                  | Description                              |
| -------------------------- | ---------------------------------------- |
| QuotationInterface         | Represents a quotation                   |
| QuotationRevisionInterface | Represents a Quotation Revision, connected to a quotation |
| QuotationUserInterface     | Represents an application user, connected to quotations and revisions |
| FormModelInterface         | Phase form model class, connected to the revision |
| ModelInterface             | Generic entity class                     |

## Manager Interfaces

To interact with the entity interfaces, there are also manager interfaces that are responsible for implementing actions on the entities. For example, the quotation manager must implement the QuotationManagerInterface that provides methods for managing quotations and revisions.

| Interface                 | Description                              |
| ------------------------- | ---------------------------------------- |
| QuotationManagerInterface | Handles loading, persisting, creating and removing quotations |
| ModelManagerInterface     | Handles loading, persisting and removing models and phase models |
| UserManagerInterface      | Handles finding and loading users        |

## Quotation Facade & Model Provider

As described in the [Quotation Facade](QuotationFacade.md) section, the Quotation Facade provides a centralized interface to managing quotations, revisions and their models.

![image](images/responsibilities.png)

The quotation facade has access to the Model Provider class, which is responsible for interacting with the quotation entities. These are atomic actions such as creating a quotation or a revision. These are delegated to the manager classes which provide the actual implementation.

![image](images/quotation_facade.png)

For simple things (such as loading a quotation), the quotation facade simply delegates the call to the model provider. But for example when creating a quotation, the facade first asks the model provider to create a quotation, then set the current user as the owner and then ask the model provider to link the quotation to the user's account.

The main difference is therefore that the Quotation Facade represents the whole quotation system and the model provider individual (database) actions.

## Loading Entities From Database

To connect the quotation entities to database, must connect to the events that load the three managers and return an object that corresponds to the interface. The diagram below explains what happens when an entity is requested.

![image](images/entity_load.png)

After the manager returns the entity, another event is also sent before validating the entity to provide access to the entity before validation (See [Events](Events.md)). 

For example, it is loaded whenever a quotation or a revision is requested. This happens with event `QuotationModelEvents::LOAD_QUOTATION_MANAGER`. The application must listen to this event and attach the quotation manager to the event:

``` php
<?php

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ModelEventSubscriber implements EventSubscriberInterface {
    public static function getSubscribedEvents() {
        return array(
            QuotationModelEvents::LOAD_QUOTATION_MANAGER => 'onQuotationManagerLoad',
        );
    }

    /**
     * @param QuotationManagerEvent $event
     */
    public function onQuotationManagerLoad(QuotationManagerEvent $event) {
        $quotationManager = new MyQuotationManager();

      	$event->setManager(quotationManager);
    }
}
```

After the quotation manager returns the quotation, another event (`QuotationModelEvents::LOAD_QUOTATION`) is also sent before validating the quotation to provide access to the quotation before validation.

The same must be done for all the managers.

| Entity Manager    | Event Name             | Event Type            |
| ----------------- | ---------------------- | --------------------- |
| Quotation Manager | LOAD_QUOTATION_MANAGER | QuotationManagerEvent |
| Model Manager     | LOAD_MODEL_MANAGER     | ModelManagerEvent     |
| User Manager      | LOAD_USER_MANAGER      | UserManagerEvent      |

**NOTE:** The KlaroQuotationExtraBundle provides these event handlers and handles entity loading automatically using Doctrine ORM.

## Model / Item Finder

To provide a way to search entities other than phase models (which are connected to revisions), the ModelManager has a method called `getItemFinder()`. The function should determine by the name given what entity is searched for and return an instance of `Klaro\Component\Entities\FinderInterface` that can be used to search for entities from the given name.

The `FinderInterface::getItems()` takes as parameter an array of filters that are used to search the items. The returned models are expected to implement the `ModelInterface` . If not, the item finder has a method `translateModel()` that is called to adapt the model to implement the interface if this is not the case.

In database terms, this translates to a query object.

``` php
<?php

$finder = $quotationFacade->getItemFinder('EntityName');

/** @var ModelInterface[] $entities */
$entities = $finder->getItems([
  'Module' => 'EM'
]);
```