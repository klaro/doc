# Web API

[TOC]

## Introduction

The Quotation Bundle includes an API interface that can be used to control and query certain aspects of the quotation process.

The full endpoint url doesn't include the API url base which by default takes the form:

``` 
[scheme]://[host]/[base_url]/api/[endpoint]
```

For example, for the `/quotation_types` endpoint:

``` 
http://localhost/quotation_app/api/quotation_types
```

## Response envelope

### Success

Envelope for successful response:

```json
{
  "data" : {...},
  "size" : 10,
  "pageSize" : 10,
  "currentPage" : 1, 
  "offset" : 0,
  "total" : 184,
  "pages" : 19
}
```

The actual response payload is always under the `data` key. Other metadata is present in adjacent keys, such as paging data for collection results.

### Errors

Errors return an envelope. The error code and message is under the `error` key and metadata is in adjacent keys (if present).

``` json
{
  "error" : {
  	"code"    : 500,
    "message" : '...'
  },
  "meta" : '...'
}
```

## Collections

### Paging 

Collection endpoint results can be paged and limited with `size` and `offset` parameters:

| Parameter | Description                              |
| --------- | ---------------------------------------- |
| size      | Maximum number of results per page       |
| offset    | Result set offset, starting with 0 as the first |

The response will include the paging information:

| Parameter   | Description                              |
| ----------- | ---------------------------------------- |
| size        | Number of results on the page            |
| pageSize    | Maximum number of results per page       |
| currentPage | Current page number                      |
| offset      | Result set offset, starting with 0 as the first |
| total       | Total number of items                    |
| pages       | Total number of pages                    |

### Sorting

Collection endpoints can be sorted by adding the `sort` parameter to the query. Not all columns are available for sorting (see each endpoint for sorting options). By default, the sort order is ascending but you can a minus sign (`-`) to the column to denote descending sort order. Eg.

Ascending sort:

```
/endpoint?sort=date
```

Descending:

```
/endpoint?sort=-date
```

To add multiple sort columns give the parameter as array:

```
/endpoint?sort[]=title&sort[]=-date
```

## Endpoints

### Quotations

#### List of quotation types

**Name:** klaro_quotation_api_get_quotation_types

``` 
GET /quotation_types
```

``` json
200 OK
"data" : {
	"product_line_1" : "Product Line 1",
    ...
}
```

#### List Quotations

**Name:** klaro_quotation_api_get_quotations

``` 
GET /quotations/{productLine}?size=&offset=&sort=
```

| Parameter     | Description    |
| ------------- | -------------- |
| quotationType | Quotation Type |

| Query Parameter | Required | Description                              |
| --------------- | -------- | ---------------------------------------- |
| userId          | No       | If present, only quotations being handled by that user are shown |
| updated_start   | No       | Show only quotations that have been updated after this date |
| updated_end     | No       | Show only quotations that have been updated before this date |
| created_start   | No       | Show only quotations that have been added before this date |
| created_end     | No       | Show only quotations that have been added before this date |

| Sort Parameter | Description         |
| -------------- | ------------------- |
| owner          | Owner user id       |
| created_at     | Time of creation    |
| updated_at     | Time of last update |

``` json
200 OK
"data" : [
  {
    "quotation_id" : 643,
    "product_line" : "product_line_1",
    "title" : "Q643",
    "owner" : 5559,
    "latest_revision" : 1,
    "created_at" : 1427786124,
    "updated_at" : 1447764143
  },
  {
    ...
  }
]
```

#### Single Quotation

**Name:** klaro_quotation_api_get_quotation

``` 
GET /quotation/{quotationId}?state={state}
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |

| Query Parameter | Required | Description                      |
| --------------- | -------- | -------------------------------- |
| state           | No       | If true, show state information. |

``` json
200 OK
"data" : {
  "quotation_id" : 1035,
  "product_line" : "flotation",
  "title" : "Q1035",
  "owner" : 5559,
  "latest_revision" : 8,
  "created_at" : 1447418405,
  "updated_at" : 1452243501
}
```

With state information:

``` json
200 OK
{
  "quotation_id" : 1035,
  "product_line" : "flotation",
  "title" : "Q1035",
  "owner" : 5559,
  "latest_revision" : 8,
  "created_at" : 1447418405,
  "updated_at" : 1452243501,
  "won" : false,
  "final" : false,
  "order" : false
}
```

#### Add handlers

**Name:** klaro_quotation_api_post_quotation_users

``` 
POST /quotation/{quotationId}/users
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |

| Post data | Description       |
| --------- | ----------------- |
| userIds   | Array of user ids |

``` json
204 OK 
```

#### Get handlers

**Name:** klaro_quotation_api_get_quotation_users

``` 
GET /quotation/{quotationId}/users
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |

``` json
200 OK
"data" : [
   {
      "id" : 5559,
      "title" : "Juha Haavisto (juha.haavisto@sujuwa.fi)",
      "fullname" : "Juha Haavisto",
      "username" : "c_juhhaa",
      "email" : "juha.haavisto@sujuwa.fi"
   }
]
```

### Revisions

#### Get list of revisions

**Name:** klaro_quotation_api_get_quotation_revisions

``` 
GET /quotation/{quotationId}/revisions
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |

``` json
200 OK
"data" : [
    {
      "quotation_id" : 1035,
      "revision_id" : 8,
      "user_id" : 5559,
      "qualifiers" : {
        "scope":"equipment",
        ...
      },
      "status" : "in_process",
      "order_status" : null,
      "log_message" : "Log message",
      "created_at" : 1451998152,
      "updated_at" : 1452243501
    },
  	{
  	   ...
    }
]
```

#### Single revision

**Name:** klaro_quotation_api_get_quotation_revision

``` 
/quotation/{quotationId}/revision/{revisionId}?state={state}
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |
| revisionId  | Revision Id  |

| Query parameter | Required | Description             |
| --------------- | -------- | ----------------------- |
| state           | No       | Show state information. |

``` json
200 OK
"data" : {
    "quotation_id" : 1035,
    "revision_id" : 8,
    "user_id" : 5559,
    "qualifiers" : {
      "scope":"equipment",
      ...
    },
    "status" : "in_process",
    "order_status" : null,
    "log_message" : "Log message",
    "created_at" : 1451998152,
    "updated_at" : 1452243501
}
```

With state information:

``` json
200 OK
"data" : {
    "quotation_id" : 1035,
    "revision_id" : 8,
    "user_id" : 5559,
    "qualifiers" : {
      "scope":"equipment",
      ...
    },
    "status" : "in_process",
    "order_status" : null,
    "log_message" : "Log message",
    "created_at" : 1451998152,
    "updated_at" : 1452243501,
    "editable" : true,
    "printable" : true,
    "won" : false,
    "final" : false,
    "order" : false,
    "latest" : true,
    "states": {
      "in_process" : "In Process",
      "active" : "Active",
      "hold" : "Hold"
    },
    "order_states":[]
}
```

#### Set revision data

**Name:** klaro_quotation_api_post_quotation_revision

``` 
POST, PUT /quotation/{quotationId}/revision/{revisionId}
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |
| revisionId  | Revision Id  |

| Post data    | Description                          |
| ------------ | ------------------------------------ |
| qualifiers   | Array of qualifiers and their values |
| status       | New status                           |
| order_status | New order status                     |
| log_message  | Log message                          |

See "Single revision" for return value.

#### Phase form data

**Name:** klaro_quotation_api_get_quotation_phase_data

``` 
GET /quotation/{quotationId}/revision/{revisionId}/phase?phaseId={phaseId}&subId={subId}
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |
| revisionId  | Revision Id  |

| Query Parameter | Required | Description                |
| --------------- | -------- | -------------------------- |
| phaseId         | Yes      | Phase id to load           |
| subId           | No       | Ordinal for repeated phase |

``` json
200 OK
"data" : {
  "Field1" : "...",
  "Field2" : "...",
  ...
}
```

#### Summary data

**Name:** klaro_quotation_api_get_revision_summary

``` 
GET /quotation/{quotationId}/revision/{revisionId}/summary
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |
| revisionId  | Revision Id  |

``` json
200 OK
"data" : {
  "quotation_id" : 1035,
  "revision_id" : 5,
  "currency" : "ZAR",
  "original_sales_price_with_tax" : 145422077.75172,
  "sales_price_with_tax" : 145422077.75172,
  "sales_price" : 145422077.75172,
  "sales_tax" : {
    10 : 12323.23,
    14 : 23123.24662
  },
  "cost_price" : 102849711.03387,
  "profit" : 42572366.717848,
  "margin" : 29.275036759227
}
```

#### Update final order price

**Name:** klaro_quotation_api_post_revision_summary

``` 
POST PUT /quotation/{quotationId}/revision/{revisionId}/summary
```

| Parameter   | Description  |
| ----------- | ------------ |
| quotationId | Quotation Id |
| revisionId  | Revision Id  |

| Post data          | Description        |
| ------------------ | ------------------ |
| custom_sales_price | Final order value. |

See "Summary data" for return value.

### Users

#### Get list of users

**Name:** klaro_quotation_api_get_users

``` 
GET /users?size=&offset=&sort=
```

| Query Parameter | Description                              |
| --------------- | ---------------------------------------- |
| first_name      | Show user whose first name matches the given. Use `%` in the beginning or end for matching likeness. |
| last_name       | Match last name, use `%` for likeness.   |
| email           | Match email, use `%` for likeness        |