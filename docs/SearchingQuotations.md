# Finding Quotations & Users

[TOC]

## Quotations

You can search for quotations using the `QuotationFacade::findQuotations()` method. The method accepts a `QuotationQuery` object which defines the searched fields, sorting and limiting the number of results.

Create a new query object with`QuotationQuery::create()`.

```php
<?php
// Find quotations from "test" product line.
$query = QuotationQuery::create()
  ->setProductLine('test');

$quotations = $facade->findQuotations($query);
```

## Users

To find users, use the `QuotationFacade::findUsers()` method with the `UserQuery` object.

```php
<?php
// Find users whose emails end with "@gmail.com"
$query = UserQuery::create()
  ->setEmail('%@gmail.com', QueryCriteria::LIKE);

$users = $facade->findUsers($query);
```

## Selected fields

Set the returned fields with `QueryInterface::setQuotationFields()` using the constants from the `QuotationQueryFields` class.

```php
<?php
$query->setQuotationFields([
  QuotationQueryFields::ID,
  QuotationQueryFields::PRODUCT_LINE
]);
```

Or get all quotation fields:

```php
<?php
$query->setQuotationFields(QuotationQueryFields::getAllFields());
```

To get data that relates to the quotation's latest revision or the user/owner, use the `QueryInterface::setRevisionFields()` and `QueryInterface::setUserFields()` methods (using the constants from `RevisionQueryFields` and `UserQueryFields` classes).

## Adding Constraints

Each query type has its own constraints, such as `QuotationQuery::setOwner()` to filter quotations based on the owner. Constraints can also have different comparisons, see the table below (defined in `QueryCriteria` class).

| Comparison    | Description                              |
| ------------- | ---------------------------------------- |
| EQUALS        | =                                        |
| NOT_EQUALS    | !=                                       |
| IN            | Value exists in given group              |
| NOT_IN        | Value does not exists in given group     |
| BETWEEN       | Value lies between given range           |
| LIKE          | Value matches a pattern, use `%` character to match any number of characters |
| GREATER_THAN  | >                                        |
| GREATER_EQUAL | \>=                                      |
| LESS_THAN     | <                                        |
| LESS_EQUAL    | <=                                       |

For simple comparisons, the  value is a single scalar value. For `IN`, `NOT_IN` and `BETWEEN` comparisons, the value must be an array.

```php
<?php
// Find quotations whose handler user id is not 1.
$query->setHandler(1, QueryCriteria::NOT_EQUALS);

// Find quotations after 2012-01-01
$query->setCreatedAt('2012-01-01', QueryCriteria::GREATER_EQUALS);
```

For the `IN` and `NOT_IN` comparisons, use an array as the value.

```php
<?php
// Find quotations with handler user ids 1 or 3
$query->setHandler([1, 3], QueryCriteria::IN);

// Find quotations, exclude from users 4, 5 and 6
$query->setHandler([4, 5, 6], QueryCriteria::NOT_IN);
```

For range searching, use `BETWEEN` with an array parameter where the first value is the start and the second is the end value.

```php
<?php
// Find quotations between '2012-01-1' and '2015-12-31'.
$query->setCreatedAt(['2012-01-1', '2015-12-31'], QueryCriteria::BETWEEN);
```

## Sorting

You can sort the results by calling `QueryInterface::addSortParam()` with the desired field name and the sort direction (from `SortParam` class). Use the `QuotationQueryFields`, `RevisionQueryFields` and `UserQueryFields` for field names. The sorted fields should be also added to the quotation, revision and user fields when selecting data.

```php
<?php
// Find quotations, sort by hander user id and date
$query = QuotationQuery::create()
  ->setQuotationFields(QuotationQueryFields::getAllFields())
  ->setUserFields([UserQueryFields::ID])
  ->addSortParam(UserQueryFields::ID, SortParam::ASCENDING)
  ->addSortParam(QuotationQueryFields::CREATED_AT, SortParam::DESCENDING);
```

## Limit Results

To limit the number of results returned by the query, use `QueryInterface::setMaxResults()`. The returned result set will then contain only that many objects. To get the total amount of possible results, call `ResultSetInterface::getTotalNumberOfResults()`.

```php
<?php
// Get only 10 quotations
$query = QuotationQuery::create()
  ->setProductLine('test')
  ->setMaxResults(10);
$quotations = $facade->findQuotations($query);

// Number of objects fetched = 10
$count = count($quotations);    // or $num = $quotations->count();

// The actual result without limit
$total = $quotations->getTotalNumberOfResults();
```

To set an offset, call `QueryInterface::setFirstResult()`. This will start the listing from the specified entry. The methods `ResultSetInterface::getCurrentPage()` and `ResultSetInterface::getNumberOfPages()` can be used to implement paging controls.

```php
<?php
// Get only 10 quotations
$query = QuotationQuery::create()
  ->setProductLine('test')
  ->setMaxResults(10)
  ->setFirstResult(40);
$quotations = $facade->findQuotations($query);

$count = count($quotations);              // = 10
$page  = $quotations->getCurrentPage();   // = 3
$pages = $quotations->getNumberOfPages(); // Total page count
```

## Listing the Results

The value returned by the `QuotationFacade::findQuotations()` and `QuotationFacade::findUsers()` methods returns an object which implements the `ResultSetInterface` interface. This object is iterable and countable.

```php
<?php
$quotations = $facade->findQuotations($query);

$numQuotations = count($quotations);

/** @var QuotationInterface $quotation **/
foreach($quotations as $quotation) {
  // ...
}
```

The value returned for each row is an object which implements the `QuotationInterface` interface (or `QuotationUserInterface` for user listing). The object will contain only values for fields which have  been selected in the query. Fields which have not been selected will return null.

To transform the result list into a another form, use the `ResultSetInterface::map()` method.

```php
<?php
$arrayList = $quotations->map(function(QuotationInterface $quotation) {
  return [
    'id'           => $quotation->getId(),
    'product_line' => $quotation->getProductLine()
  ];
});
```