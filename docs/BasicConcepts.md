# Basic Concepts

[TOC]

## Quotation

A quotation represents an offer being made to a customer. Quotations consist of revisions that are include the actual data for the quotation. All quotations will start with one revision that includes the set of data input forms and offering rules required to build an offering.

## Handlers

Handlers are users who are are responsible for editing a particular quotation. Whenever a user is a  handler, he or she will see the proposal under their account and are able to edit it. 

## Revision

Revisions consist of a set of forms whose intention is to provide data to build the finished offering according to business logic rules. Usually this will consist of some basic information, such customer data and initial values which are used to build the offering.

Revisions and their contents are snapshots in time so any changes made to the forms will not be visible before creating a new revision. Revisions can be started from the latest or any point in history but only the latest revision will be visible. Any data in the previous revision will also be carried to the next revision.

### Revision Status

Revision status keeps track the revision (and quotation) state.

| Status     | Description                              |
| ---------- | ---------------------------------------- |
| In Process | Initial state, revision data is modifiable |
| Active     | Revision is ready and sent to customer   |
| Won        | Quotation has been won                   |
| Hold       | Quotation is put on hold for a time      |
| Lost       | Quotation has been lost                  |

Below is the diagram of the state machine:

![image](images/status.png)

> **IP** = In Process, **A** = Active, **H** = Hold, **L** = Lost, **W** = Won

To move from states with no output states, you have to create a new revision. In other words, moving to **Won** state from **In Process** is possible without creating a new revision but not from **Won** to **In Process**.

### Orders

When a revision has been won (set to **Won** status), it can be copied to order. In essence, this means that the customer has approved the offering has ordered the offered goods. However, it often happens that the order might change in some parts or the final price needs to be adjusted. Therefore, orders have a separate order status of their own to keep track of order changes.

| Order Status | Description                              |
| ------------ | ---------------------------------------- |
| Not Ready    | Quotation won but yet copied to order    |
| In Process   | Copied to order                          |
| Finished     | Customer has accepted the order, no more modifications are allowed |

Diagram:

![image](images/order-status.png)

> **NR** = Not Ready, **IP** = In Process, **F** = Finished

**Note:** Only the latest revision may be in finished state.

### Qualifiers

Qualifiers are a set of configurable labels that can be applied to any revision in order to classify it.

## Forms

Forms (referred to as phases internally) are a collection pages to gather the necessary user input needed to form the offering based on business logic rules. 

### Form Model

Represents one model which is linked to a quotation revision form phase. The data from the form is saved in to this model. The model is always linked to the revision and phase id. The [application model manager](DatabaseConfiguration.md) is responsible for finding the correct item and if the model does not exist, it should create a new entry.

### Model / Item Finder

Model is representation of a database entry with a single identifier (ie. generic entity class). The difference to form models is that they are not tied to any revision or phase. These models may be used in the forms for examples as options in dropdown.

To search models, a finder is needed. This is a class through which form models can be searched and listed and it should return a collection of items that implement the model interface.

## Offering Summary

Offering is the result of applying the business logic rules based on the user input. By default, this is a hierarchical structure of sections and sale items. Sale items have a cost price and a sales price from which to the total price and margin of the quotation is calculated.