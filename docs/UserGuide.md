# User Guide

[TOC]

## Product Line List

Shows a list of configurable products. Click the product line name to see a list of quotations or start a new quotation for that product line.

## Quotation List

On this page you will see a list of existing proposals. 

At this point, you may choose to:

- Start a new proposal by clicking New Proposal button on the top right corner. If there are more than one proposal, you can click "New Proposal (for other)" to select a product line.
- Browse own quotation (where the current user is one of the handlers)
- Continue editing an existing proposal by clicking the quotation on the list

## Quotation Info

Clicking any quotation from the list takes you to the detailed info page. It shows information about the quotation, its handler and the revisions. You can:

- Edit Quotation. To start editing the latest revision of the quotation, click “Edit proposal” button.
- Add handlers. You can add handlers to your proposal so those users can also see the quotation under their account and are able to edit it.
- Make a new copy of the quotation.
- Add yourself as a handler to the quotation.
- Claim the quotation for yourself, meaning you will become the only handler.
- Start new revision
- Remove the quotation

Some of these actions might not be available, depending on the user permissions.

If you want to edit other user’s proposal you need to ask owner of the proposal to add you as a handler of proposal. When you are handler you can see the proposal under your account and are able to edit it.

## Edit Quotation

The structure of configurator consists of:

- Left  Navigation menu:
  - Is used to navigate between different pages of configurator. Different input forms are shown under "Offering".
  - Start from “General Information” and proceed towards “Offering Summary”
  - Red exclamation marks after page  means that pages are not locked (prevents access to Offering Summary).
- Side bar:
  - Contains useful information related to page you are working on.
  - Appearance and content may vary, by default it shows the current offering summary
- Navigation bar
  - Is used to navigate between different sheets.
  - Number of sheets is varying between different pages.
  - “Lock / Unlock” button is also located here. Locking is mandatory action for those pages where lock exists. Lock is needed because of technical reasons (Runs and saves calculations on background).

## How to edit or copy your existing Proposals

To make a copy of your existing proposal, go the quotation's info page and click “Copy under own account” button.

## How to copy other users Proposals

- In the quotation list page, click “Other user´s proposals” icon and find the proposal you want to edit or copy.
  
- You can copy it under your own account by clicking “Copy under own account” button.
  
- Copying makes a hard copy of the proposal. The copies are not in any way tied together. Use this option to start new proposal based on another proposal.
  
  ​