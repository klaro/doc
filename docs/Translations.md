# Translations

[TOC]

## Introduction

Symfony provides a [translation component](http://symfony.com/doc/current/book/translation.html) that allows applications and bundles to define translations which can be overridden in the application level.

QuotationBundle uses translations from the domain `KlaroQuotationBundle`. Translation files are in `@KlaroQuotationBundle/Resources/translations/` . 

Currently, these translation files exist: 

- English (KlaroQuotationBundle.en.yml)
- Finnish (KlaroQuotationBundle.fi.yml)

To change the used language, enable the translator and set the language as described in the Symfony guide.

``` yaml
# app/config/config.yml
framework:
    translator: { fallbacks: [en] }
```

These can be overridden locally in the application by creating a file with the same name to your app or bundle Resources folder. 

Symfony looks for message files (i.e. translations) in the following default locations:

- the `app/Resources/translations` directory;
- the `app/Resources//translations` directory;
- the `Resources/translations/` directory inside of any bundle.

The locations are listed here with the highest priority first. That is, you can override the translation messages of a bundle in any of the top two directories.

## Using Translations

To use translations defined in the Quotation Bundle, you need the use message keys defined in the  files and use the domain `KlaroQuotationBundle`.

### In Twig templates

Translating work with the `trans()` filter in Twig templates.

``` 
{{ 'edit.alerts.general'| trans({}, 'KlaroQuotationBundle') }}
```

### In PHP

For PHP, you have to inject or get the translator service from the service container.

``` php
<?php

/** @var TranslatorInterface $translator */
$translator = $this->get('translator');

$translated = $translator->trans('edit.alerts.general', [], 'KlaroQuotationBundle');
```

### In JavaScript

Translations are exposed to JavaScript files with the help of [JsTranslationBundle](Configuration.md). To use, require the translator object in the module definition.

``` javascript
define(['translator'], function(Translator) {
  var translated = Translator.trans('edit.alerts.general', {}, 'KlaroQuotationBundle');
});
```

## Customizing terminology

Translation files can also be used to customize the terminology. By default, the Quotation Bundle refers to quotations and revisions but these can be changed to something else like proposals and versions respectively. 

For example, to override the english terms, create a custom translation file as described above (eg. `/app/Resources/translations/KlaroQuotationBundle.en.yml`). Then replace the needed terms in the file:

``` yaml
general:
    layout:
        title_multiple:         Proposals
        title_single:           Proposal
        specifier:              for %productLine%

revision:
    title:   					Version
```

