# Event System

The Quotation Bundle makes extensive use of [Symfony's event handling system](http://symfony.com/doc/2.3/components/event_dispatcher/introduction.html).

The quotation bundle is not tied to DBAL implementation since it only uses different interfaces to interact with the surrounding system. Events are used to query all quotation, models and users. It is up to the user to implement these interfaces and write the glue code to provice the appropriate data to the quotation bundle.

### Subscribing to events

To subscribe to an event, [implement an event listener as described in the symfony website](http://symfony.com/doc/2.3/components/event_dispatcher/introduction.html).

Add the service:

``` json
# config.yml

services:
    my_project.quotation_event.listener:
        class: ETC\MyBundle\Subscriber\QuotationEventSubscriber
        arguments: [...]    # pass any arguments you might need
        tags:
            - {name:kernel.event_subscriber}
```

Implement the `EventSubscriberInterface` interface and return the events you want to subscribe to:

``` php
<?php
// ETC\MyBundle\Subscriber\QuotationEventSubscriber.php

class QuotationEventSubscriber implements EventSubscriberInterface {
    public static function getSubscribedEvents() {
        return array(
            QuotationEvents::QUOTATION_SIDEBAR => 'onQuotationSidebar',
            QuotationEvents::QUOTATION_SUMMARY => 'onQuotationSummary',
            // ...
        );
    }

    public function onQuotationSidebar(QuotationEvent $event) {
        // ...
    }

    public function onQuotationSummary(QuotationEvent $event) {
        // ...
    }
}
```

### Event types

Different events different event types. Below is the class diagram for events.

![image](images/events.png)

Most events inherit the `Klaro\QuotationBundle\Event\QuotationEvent` and `Klaro\QuotationBundle\Event\QuotationRevisionEvent` class and so you can always access the current quotation or revision (if one is active - otherwise it is NULL). All events are inherited from `Klaro\QuotationBundle\Event\ResponseEvent` which in turn inherits from Symfony's Event class. This allows to set a response to the event that can be used to override the content returned by the controller in controller events.

### Events

Refer the to the event class documentation for each event. The event classes are under the namespace `Klaro\QuotationBundle\Event`. Depending on the event, there may be other data associated with the event, or you might have to set some data to the event. This can be done by calling the `QuotationEvent::getData()`, `QuotationEvent::setData()` and `QuotationEvent::addData()` methods. These are documented separately for each event.

| Event class               | Description                              |
| ------------------------- | ---------------------------------------- |
| DocumentEvents            | Document events are triggered when accessing documents under Offering Summary print options. |
| MenuEvents                | Menu events are triggered when creating menu items for editing quotation phases. |
| PhaseEvents               | Defines events generated during different form phases. |
| QuotationControllerEvents | Defines UI events triggered during the proposal process. Possible to override the response. |
| QuotationFacadeEvents     | Defines events that are triggered during the quotation management process. |
| QuotationModelEvents      | Define events that modify the quotation entities. |
| SummaryPageEvents         | Offering summary page events.            |
| UserEvents                | Events related to user handling.         |
| ValidationEvents          | Events which happens when validating the offering summary page. |

