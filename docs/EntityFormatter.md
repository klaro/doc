# Entity Formatter

[TOC]

The Entity Formatter is a service which is responsible for formatting quotation, revision and user titles. The entity formatter implements the interface in `Klaro\QuotationBundle\Api\EntityFormatterInterface` and can be accessed with the service id `klaro_quotation.entity_formatter`.

You can create your own formatter by creating a service which implements the `EntityFormatterInterface` interface and tagging it with the name `klaro_quotation.entity_formatter`. 

```yaml
services:
    my_app.entity_formatter.custom:
        class: Acme\MyBundle\EntityFormatter\CustomEntityFormatter
        tags:
            - { name: 'klaro_quotation.entity_formatter', alias: 'my_custom_formatter' }
```

The alias is used to refer to the service in the [configuration](Configuration.md). To use the custom formatter, set the custom alias as the value to the `klaro_quotation.formatter` key.

**NOTE:** Only one formatter is in use at a time.

