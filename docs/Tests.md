# Tests

[TOC]

## Running tests

To run tests, go to the Quotation Bundle directory and first install the required dev components:

``` 
php composer.phar update --require-dev
```

The run the test suite:

``` 
make test
```

## Adding tests

Tests are written with PHPUnit and are located in `Klaro\QuotationBundle\Tests`.

