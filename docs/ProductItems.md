# Product Sales Items

[TOC]

## Introduction

Product (sales) items refer to items in the product database that can be matched to specific sales structure rows based on interface selections. The product item array consists of sections under the `productItems` key. Section names should match the section keys defined in the [OfferingSummary](OfferingSummary.md) page. 

Not all sections have to be defined, only the ones that should contain product items. For each matched key in the sales structure, product items are searched from the product database with the given filters with the aim of producing one (or in some cases more) product item. The product item's base data (item code, description, price) is copied into a so called conf item and that can be modified to have prices and quantities based on UI selections.

For each section, you can define what is the source product database, which fields are copied from the sale item to the conf item and which values are evaluated. Usually these are the same for many sections, so it is easier to define a `defaults`section before `productItems` to set them for all. All default values can be overridden in the section configs.

Product item structure: 

``` yaml
defaults:
    source:           [...]
    filters:
        [filter1]:    [...]
        ...
        [filterN]:    [...]
    copy:
        [field]:      [Field]
    values:
        [field]:      [...]

productItems:
    [section]:
        source:           [...]
        filters:
            [filter1]:    [...]
            ...
            [filterN]:    [...]
        copy:
            [field]:      [Field]
        values:
            [field]:      [...]
```

## Options

| Option  | Type    | Optional | Description                              |
| ------- | ------- | -------- | ---------------------------------------- |
| source  | string  | Yes      | Name of the product item database query (object that implements the FinderInterface). |
| create  | boolean | Yes      | Set this flag to true if you want to create a conf item even when no product items are found or you do not use product items). The conf item's data is filled with keys from the `values` section. |
| filters | array   | Yes      | Filters to narrow down the product database search. |
| copy    | array   | Yes      | List of fields to copy over to the conf item. Array key defines the destination field and the value is the source |
| values  | array   | Yes      | Values which are set in the conf item. Array key defines the destination field and the value is the desired value or evaluated condition. |

## Filters

Filters is an array of key-value pairs where the key defines the product database column and value is the search condition value.

The following objects are available while evaluating conditions:

| Object    | Description                              |
| --------- | ---------------------------------------- |
| model     | The current phase model                  |
| models    | Model provider, use `models.ModelName.Field` to access fields from other models. If a phase is repeatable, then you have to use `models["ModelName"][index]` to get the correct sub model. By default, the first sub model is always returned. |
| quotation | The current quotation, use like `quotation.ProductLine == "FP"` |
| user      | The current user, use like `user.FullName` |
| this      | Reference to the current section, use `this.parent` or `this.parent("parentSection")` to access sections in the hierarchy. |
| item      | Only valid for `values` key. Reference to found product item. |

## Copied and evaluated values

In the `copy` section, values are copied to the conf item by calling `confItem.set("Destination", productItem.get("Source"))` on the product item where `Source` is the product item field to copy and `Destination` is the target field. If no product items are found, no conf item is created unless `create` field is set to true (values are copied only from found product items).

In the `values` section, values are set to the conf item by calling `confItem.set("Destination", evaluate(condition))` where `condition`is the value or condition provided in the array and `Destination` is the target field (array key). 

The following properties are needed to generate summary information:

| Property      | Description                              |
| ------------- | ---------------------------------------- |
| InternalTitle | Internal Title (shown in summary's "Item name" column) |
| ExternalTitle | External Title (shown in commercial quotation) |
| ItemCode      | Item code                                |
| Quantity      | Unit Quantity                            |
| CostPrice     | Unit Cost Price                          |
| SalesPrice    | Unit Sales Price                         |
| Notice        | Notice to show messages for conf item    |
| SalesTax      | Sales Tax                                |

## Metadata

You can set any data to a conf item and it will be available when traversing the sales structure. You can use any other key other than the ones defined in the previous section. For example to set or get a field name `PriceType`, use `$confItem->set('PriceType', 'SINGLE')` and `$confItem->get('PriceType')` respectively.

Example:

``` yaml
# the defaults apply to all items
defaults:
    source:              OSCAutomationSaleItemQuery
    copy:
        ItemCode:        ItemCode
        InternalTitle:   Title
        ExternalTitle:   Title    # copy the same field to both internal and external title

productItems:
    ImagerAssembly:
        # look for product item with item code 40
        filters:
            ItemCode:     40

        # set some extra values for this item
        values:
            Notice:       '"This is a notice shown in the summary"'
            Type:      	  COMPONENT
```

To do this programmatically, use the `set()` and `get()` methods:

``` php
<?php

$confItem = $section->createConfItem();
$confItem->set('Type', 'COMPONENT');

// use it later:

$type = $confItem->get('Type');
```