# Form Phase Definition

[TOC]

## Introduction

Each configurable product consists of input phases or forms that gather data to be used when building the offering summary. For each phase in the [product line configuration](Configuration.md), you must create a YAML file that defines the phase config for that phase.

## Phase definition config

Each phase file must define a `phaseDefinition` array. The configuration is designed to be to used with the [Phase Form Editor](PhaseFormEditor.md) but you can define custom editors as well. In those cases defining items in the config file might not make sense. 

Here it is assumed that the phase form editor is being used to input phase data.

| Option   | Type              | Required | Notes                                    |
| -------- | ----------------- | -------- | ---------------------------------------- |
| title    | string            | yes      | Phase title                              |
| extends  | string            | no       | Parent phase file definition. Values are taken from the extended file and merged with the values in the current file. See "Inheritance" below. |
| use      | array             | no       | List of namespace shortcuts. See "Namespace Shortcuts" below. |
| model    | string            | no       | Name of the model to which the form fields refer |
| icon     | string            | no       | Font icon name                           |
| repeat   | int/condition     | no       | Number of times this phase should be repeated. This value can depend on another model field, making it dynamic. |
| lockable | boolean/condition | no       | Allow locking of of phase and selected fields. If phase is lockable then the fields with attribute `locked` are highlighted with a "required" icon. Also, the summary is not shown until all lockable phases are locked. Locking can be defined as condition based on other models' data. |
| disabled | boolean/condition | no       | Allow a phase to be disabled with true/false or condition. Disabling a phase prevents access to it but it is still shown in the menu as a disabled option. |
| phases   | array             | no       | List of subphases which are show as tabs inside this phase. Consists of key-value pairs where the array key defines the id of the phase and the value provides the path to the phase definition file (similar to product line config). See "Subphases" below. |
| items    | array             | yes      | List of editable form fields.            |
| template | string            | no       | Override the default phase template, eg. `AcmeBundle:Controller:phaseParallel` |
| editor   | string            | no       | Defines the editor for this phase, which by default the phase form editor (`phase`). See the [phase form editor config](PhaseFormEditor.md) for more details. |
| ordering | array             | no       | A list of keys that are also defined in the `items` array. If present, the keys are used to filter the items array and order the items array. This is useful if you extend another config and want to reorder the items array (or leave some items out). |

### Extending phases

You can extend the phase from another by giving the parent phase path in the `extends` parameter. For example, assuming there is a file called `parent.yml` with content like this:

``` yaml
phaseDefinition:
	title:		Parent Phase
    icon:		arrow
    lockable:	false
	items:
    	first:
        	text:	"First field"
            type:	text

        second:
        	text:	"Second field"
			type:	text
```

And another:

``` yaml
phaseDefinition:
	title:		Child Phase
    extends:	/path/to/parent/parent.yml
    lockable:	true
    items:
    	third:
        	text:	"Third field"
            type:	text
```

Then the resulting phase config will look like this:

``` yaml
phaseDefinition:
	title:		Child Phase
    icon:		arrow
    lockable:	true
    items:
    	first:
        	text:	"First field"
            type:	text

        second:
        	text:	"Second field"
			type:	text

		third:
        	text:	"Third field"
            type:	text
```

The phase parameters which exist in the parent phase are preserved unless overridden in the child. In the above example, the `lockable` property is `true` because it is overridden in the child. 

Items are combined. To hide an item from the parent in the child phase, set its key to null like this:

``` yaml
phaseDefinition:
	title:		Child Phase
    extends:	/path/to/parent/parent.yml
    lockable:	true
    items:
    	second: ~

    	third:
        	text:	"Third field"
            type:	text
```

This would only show the fields "First field" and "Third field" in the resulting page.

### Ordering Items

You can control the ordering of items with the `ordering` parameter. This is mainly useful if you are extending from another phase file config and want to control the order in which the items are shown. The values refer to the keys that are defined in the `items` array.

``` yaml
phaseDefinition:
	title:		Child Phase
    extends:	/path/to/parent/parent.yml
    lockable:	true
    items:
    	third:
        	text:	"Third field"
            type:	text
    ordering:
    	- third
		- first
		- second
```

This will show the fields in order: "Third field", "First field" and "Second field".

### Namespace Shortcuts

Models for phases are searched by their name with events (see [Events](Events.md)). The surrounding app is responsible for instantiating models based on the names defined in the phase definition config. Eg. for a phase with model name "General", a corresponding model should be returned. 

The model name may also be a complete path with namespace which might help with finding the correct model. However, typing the full namespace every time is tedious. With the `use` parameter, you can define shortcuts that are replaced with the corresponding value from the `use` array.

For example if there are these shortcuts defined:

``` yaml
phaseDefinition:
	use:
    	General:	Acme\SomeBundle\Entity\General
        Delivery:	Acme\SomeBundle\Entity\Delivery
    model:	General
```

In this case, when searching for the phase model ("General"), the value is replaced with the entry from the `use` array ("Acme\SomeBundle\Entity\General"). 

### Subphases

An example of a phase definition config might look like this:

``` yaml
# phaseFile.yml
phaseDefinition:
    title:                    Phase title
    model:                    SomeModelName
    icon:                     list
    repeat:                   "models.SomeOtherModelName.NumberOfPhases"
    lockable:                 true
    items:
```

To configure subphases, set the sub phase paths under `phaseDefinition.phases`. The path should be the full path to the file in respect to the project root folder.

``` yaml
# phaseFile.yml
phaseDefinition:
    title:                    Phase title
    model:                    SomeModelName
    icon:                     list
    phases: 
        subPhase1:     src/OSC/FlotationBundle/Resources/data/phases/subPhase/subPhase1.yml
        subPhase2:     src/OSC/FlotationBundle/Resources/data/phases/subPhase/subPhase2.yml
        subPhase3:     src/OSC/FlotationBundle/Resources/data/phases/subPhase/subPhase2.yml
```

Subphases are then shown as tabs in the main phase. The main phase does not have any content but the first subphase becomes the content shown when accessing the main phase page.

If subphases are not defined, the editor fields must be defined under `phaseDefinition.items`. Editor items are array nodes under `items`. The index of each form element must be unique. Eg.:

``` yaml
# phaseFile.yml
phaseDefinition:
    items:
        sample:
            ...
        anotherField:
            ...
```

The exact definition depends on the editor being used. See the configuration options for each editor separately. By default, only the phase form editor is available. To enable custom editors in your project, see "Phase Editors" in [Extending Services](ExtendingServices.md).

## Loading the editor

Below is what happens when the phase is loaded in the controller.

![image](images/phase.png)

Below is the class diagram for the phase editor and related classes.

![image](images/phase_uml_simple.png)

## Editor Services

It is possible to implement a custom editor to edit a phase. The default editor is the [Phase Form Editor](PhaseFormEditor.md) whose type is `phase`. To use a different editor, set the editor property in the [phase file configuration](PhaseReference.md). 

Editors can add by creating a service and tagging it with the name `klaro_quotation.phase_editor`. The editor should implement the interface in `Klaro\QuotationBundle\Api\PhaseEditorInterface`.

## Phase Form Editor

See the [Phase form editor](PhaseFormEditor.md) for usage.