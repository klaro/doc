# Docker Installation

[TOC]

The docker installation is nowadays preferable.

Manual way to install the project is [also possible](Installation.md).

## Installing

Clone the repo with it's submodules: 

```
git clone --recursive git@gitlab.com:klaro/docker-demo.git
```

Copy the `.env.example` as an `.env` file.

You'll need to pass your id_rsa key into the container to get cloning from gitlab working there:

```bash
export PRIVATE_KEY=`cat "$HOME"/.ssh/id_rsa`
docker-compose build --build-arg PRIVATE_KEY="$PRIVATE_KEY" klaro-demo
```

Another option is to add the key into the container as `/root/.ssh/id_rsa` file manually later.

## Running

On MacOS you'll need `docker-sync` installed and running (with `docker-sync start)` before to get the source files
syncing between the host and container (if needed).

The `docker-compose up` should be enough for running everything if above install instructions were made.

## Creating database tables and user

In order to execute provided migrations, do the following:

```bash
docker-compose exec klaro-demo /usr/local/bin/php /opt/projects/klaro-demo/bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration
```

To create a user with admin privileges run the following (username: admin, password: test):

```bash
docker-compose exec klaro-demo /usr/local/bin/php bin/console fos:user:create admin admin@cpq3-demo.com test
docker-compose exec klaro-demo /usr/local/bin/php bin/console fos:user:promote admin ROLE_ADMIN
```

## Usage

The project should be accessible in browser with `http://liveapps.local/klaro-demo/` now.

Follow doc.klarocpq.com to get further instructions. The `projects/klaro-demo/README.md` file can also be useful.
