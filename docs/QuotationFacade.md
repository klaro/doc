# Quotation Facade

[TOC]

## Introduction

QuotationFacade provides a centralized interface to managing quotations, revisions and their models. To interact with the quotation bundle, you can get the Quotation Facade service with the id `klaro_quotation.quotation_facade` like this:

``` php
<?php
$quotationFacade = $container->get('klaro_quotation.quotation_facade');
```

## Methods

Below are some examples on how to use the QuotationFacade. For more information, see the `Klaro\QuotationBundle\Facade\QuotationFacade` class.

### Users

To get the current user, call `QuotationFacade::getCurrentUser()`.

``` php
<?php
$user = $quotationFacade->getCurrentUser();
```

To get any user, call `QuotationFacade::getUser()` with the desired username.

``` php
<?php
$user = $quotationFacade->getUser(1234);
```

Get a list of all users with `QuotationFacade::getUsers()`.

``` php
<?php
$users = $quotationFacade->getUsers();
```

### Quotations and Revisions

#### Create Quotation

To create a new quotation, call `QuotationFacade::createQuotation()` with the owning user and product line name. The result is an instance of the created quotation's first revision.

``` php
<?php
$newRevision = $quotationFacade->createQuotation($user, 'my_product');
```

#### Create Revision

To start a new revision, call `QuotationFacade::createQuotationRevision()`. By default, the latest revision of the quotation is used as the base for copying data to the new revision.

``` php
<?php
// Start new revision from the latest revision.
$newRevision = $quotationFacade->createQuotationRevision($quotation, $user);
```

You can also start a revision based on another revision by giving the revision id as the third parameter:

``` php
<?php
// Start revision based on the fourth revision.
$newRevision = $quotationFacade->createQuotationRevision($quotation, $user, 4);
```

#### Fetching

Get quotation info with `QuotationInterface::getQuotation()`.

``` php
<?php
// Get Q1
$quotation = $quotationFacade->getQuotation(1);
```

To fetch a single revision, call `QuotationInterface::getQuotationRevision()`.

``` php
<?php
// Get Q1 revision 4
$quotation = $quotationFacade->getQuotation(1);
$revision  = $quotationFacade->getQuotationRevision($quotation, 4);
```

For the latest revision, call `QuotationInterface::getLatestRevision()`.

``` php
<?php
$latestRevision = $quotation->getLatestRevision();
```

To fetch  a list of all revisions, call `QuotationFacade::getQuotationRevisions()`.

``` php
<?php
$revisions = $quotationFacade->getQuotationRevisions(quotation);
```

#### Handlers

You can get the list of handlers for a quotation with `QuotationInterface::getLinkedUsers()` . 

``` php
<?php
$handlers = $quotationFacade->getLinkedUsers($quotation);
```

Add a user as handler with `QuotationInterface::linkQuotation()`.

``` php
<?php
// Set user with id 1 as handler for Q1.
$quotation = $quotationFacade->getQuotation(1);
$user      = $quotationFacade->getUser(1);

$quotationFacade->linkQuotation($quotation, $user);
```

To set a user as the only handler, call `QuotationInterface::claimQuotation()`.

``` php
<?php
// Set user with id 1 as the only handler for Q1.
$quotationFacade->claimQuotation($quotation, $user);
```

#### Copying

Copying quotations is done with `QuotationInterface::copyQuotation()`. This copies all revisions and related data except handlers. The given user is set as the owner and the only handler for the new quotation.

``` php
<?php
// Make a copy of Q1 under given user's account.
$oldQuotation = $quotationFacade->getQuotation(1);
$newQuotation = $quotationFacade->copyQuotation($oldQuotation, $user);
```

#### Removing

To remove a quotation and all its revisions, call `QuotationFacade::removeQuotation()`.

``` php
<?php
// Remove quotation Q1
$quotation = $quotationFacade->getQuotation(1);
$quotationFacade->removeQuotation($quotation);
```

To remove one revision, call  `QuotationFacade::removeQuotationRevision()`.

``` php
<?php
// Remove revision 4 from Q1
$quotation = $quotationFacade->getQuotation(1);
$revision  = $quotationFacade->getQuotationRevision($quotation, 4);

$quotationFacade->removeQuotationRevision(revision);
```

### Phases and Models

To get a list of all phases for a revision, call `QuotationFacade::getPhaseDefinitions()`.

``` php
<?php
// Get phases for Q1 revision 4.
$quotation = $quotationFacade->getQuotation(1);
$revision  = $quotationFacade->getQuotationRevision($quotation, 4);

$phases = $quotationFacade->getPhaseDefinitions(revision);
```

Phases are identified by phase ids which are the array keys defined in the product phase configuration (see [Configuration](Configuration.md)). Phase data is saved to models which are defined in the phase definition config. 

To get a single phase definition, call `QuotationFacade::getPhaseDefinition()`. 

``` php
<?php
// Get "general" phase model from Q1 revision 4.
$quotation = $quotationFacade->getQuotation(1);
$revision  = $quotationFacade->getQuotationRevision($quotation, 4);

$generalPhase = $quotationFacade->getPhaseDefinition($revision, 'general');
```

To get an instance to the actual model, use `QuotationFacade::getPhaseModel()`.

``` php
<?php
$modelName = $generalPhase->getModel();

$model = $quotationFacade->getPhaseModel($revision, modelName);
```

### Saving Changes

To save any changes to quotations, revisions or phase models, they must be persisted. This is done by calling `QuotationFacade::persistQuotation()`, `QuotationFacade::persistQuotationRevision()` and `QuotationFacade::persistModel()`  respectively.

``` php
<?php
// Change updated date on Q1 and save to database.
$quotation = $quotationFacade->getQuotation(1);
$quotation->setUpdatedAt(new DateTime());

$quotationFacade->persistQuotation($quotation);
```

``` php
<?php
// Set status for Q1 revision 4
$quotation = $quotationFacade->getQuotation(1);
$revision  = $quotationFacade->getQuotationRevision($quotation, 4);
$revision->setStatus(QuotationRevisionStatus::ACTIVE);

$quotationFacade->persistQuotationRevision($revision);
```

``` php
<?php
// Change data in general phase.
$generalPhase = $quotationFacade->getPhaseDefinition($revision, 'general');
$model = $quotationFacade->getPhaseModel($revision, $generalPhase->getModel());

$model->set('SomeField', 'Quick brown fox');

$quotationFacade->persistModel($model);
```