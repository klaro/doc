# User Roles

[TOC]

## Introduction

Access to different parts of the quotation bundle is restricted with specific roles. To enable a feature for user, add the role to his or her group in your `security.yml` config.

Below is a list of roles used by the quotation bundle.

## Configuration

To configure permissions, add them to the project's `security.yml` file's role hierarchy. For example:

``` yaml
security:
    role_hierarchy:
        ROLE_SALES:
            - ROLE_KLARO_QUOTATION_ACCESS_SALES_PRICE
            - ROLE_KLARO_QUOTATION_SEE_OTHER_QUOTATION
            - ROLE_KLARO_QUOTATION_CREATE_QUOTATION
            - ROLE_KLARO_QUOTATION_ACCESS_TOTAL_SALES_PRICE
            - ROLE_KLARO_QUOTATION_ACCESS_TOTAL_COST_PRICE
            - ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT
            - ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT_MARGIN
            - ROLE_KLARO_QUOTATION_ACCESS_SUMMARY_HIERARCHY
            - ROLE_KLARO_QUOTATION_OVERRIDE_ORDER_PRICE
            - ROLE_KLARO_QUOTATION_MANAGE_QUOTATION_USERS
            - ROLE_KLARO_QUOTATION_ACCESS_USERS
		ROLE_SALES_MANAGER:
        	- ROLE_SALES
            - ROLE_KLARO_QUOTATION_ACCESS_ITEM_CODE
            - ROLE_KLARO_QUOTATION_ACCESS_COST_PRICE
            - ROLE_KLARO_QUOTATION_ACCESS_SUMMARY_HIERARCHY_PRICES
        ROLE_ADMIN:
            - ROLE_KLARO_QUOTATION_PROFILER
            - ROLE_KLARO_QUOTATION_LINK_QUOTATIONS
            - ROLE_KLARO_QUOTATION_DELETE_QUOTATIONS
            - ROLE_KLARO_QUOTATION_ACCESS_ADMINISTRATION
            - ROLE_KLARO_QUOTATION_CLAIM_QUOTATIONS
            - ROLE_KLARO_QUOTATION_ACCESS_FACTORS
            - ROLE_KLARO_QUOTATION_EDIT_ALL_QUOTATIONS
```

## Permissions

### Administration

- **ROLE_KLARO_QUOTATION_ACCESS_ADMINISTRATION**

User with this role has access to administration panel and can see the administration link in the toolbar.

### General

- **ROLE_KLARO_QUOTATION_PROFILER**

User can see the profiler toolbar used with debugging information (this assumes the the toolbar is configured in your `config.yml`.

### Quotation Management

- **ROLE_KLARO_QUOTATION_SEE_OTHER_QUOTATION**

User has right to see quotations created by other users

- **ROLE_KLARO_QUOTATION_CREATE_QUOTATION**

User has right to create a quotation           

- **ROLE_KLARO_QUOTATION_LINK_QUOTATIONS**

User has right to link himself or herself to a quotation, ie. add the current user as one of the handlers of the current quotation.

- **ROLE_KLARO_QUOTATION_EDIT_ALL_QUOTATIONS**

User is able to edit quotations without adding him or herself as a handler in a quotation.

- **ROLE_KLARO_QUOTATION_CLAIM_QUOTATIONS**

User has right to claim a quotation for himself or herself. This means the current user is set as the only handler of the current quotation.

- **ROLE_KLARO_QUOTATION_DELETE_QUOTATIONS**

User with this role has right to delete quotations.

- **ROLE_KLARO_QUOTATION_ACCESS_USERS**

User has right to see a list of all users.

- **ROLE_KLARO_QUOTATION_MANAGE_QUOTATION_USERS**

User can manage quotation handlers, ie. user linked to any quotation.

### Offering Summary

These roles control what prices/features users can see in the summary.

- **ROLE_KLARO_QUOTATION_ACCESS_SUMMARY_HIERARCHY**

User can see nested levels in the summary hierarchy beyond the limit set in the parameter `klaro_quotation.summary_level`.

- **ROLE_KLARO_QUOTATION_ACCESS_SUMMARY_HIERARCHY_PRICES**

User can see the prices in nested levels in the summary hierarchy beyond the limit set in the parameter `klaro_quotation.summary_level`. User needs the ROLE_KLARO_QUOTATION_ACCESS_SUMMARY_HIERARCHY permission to see the whole hierarchy.

- **ROLE_KLARO_QUOTATION_ACCESS_FACTORS**

User can see the factors affecting a price if any factors are set.

- **ROLE_KLARO_QUOTATION_ACCESS_TOTAL_SALES_PRICE**

User can see the total sales price of the offering summary.

- **ROLE_KLARO_QUOTATION_ACCESS_TOTAL_COST_PRICE**

User can see the total cost price of the offering summary.

- **ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT**

User can see the total profit amount from the offering summary.

- **ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT_MARGIN**

User can see the total profit margin from the offering summary.

- **ROLE_KLARO_QUOTATION_ACCESS_COST_PRICE**

User can see individual cost prices in the summary.

- **ROLE_KLARO_QUOTATION_ACCESS_SALES_PRICE**

User can see individual sales prices in the summary.

- **ROLE_KLARO_QUOTATION_ACCESS_ITEM_CODE**

User can see item codes in offering summary.

- **ROLE_KLARO_QUOTATION_OVERRIDE_ORDER_PRICE**

User can override the calculated revision sales price (applies for revisions which have been copied to orders).

* **ROLE_KLARO_QUOTATION_COMPARE_QUOTATIONS**

User can compare prices between revisions in the summary page.